﻿using System;

public class Root
{
    public Former former { get; set; }
    public Group[] groups { get; set; }
    public Diplomate[] diplomates { get; set; }

    public Group getGroupById(int id)
    {
        foreach (Group g in groups)
        {
            if (g.id == id)
            {
                return g;
            }

        }
        return null;
    }
    public Diplomate getDiplomateById(int id)
    {
        foreach (Diplomate d in diplomates)
        {
            if (d.diplomate_id == id)
            {
                return d;
            }

        }
        return null;
    }

    public List<string> getEvidences(int groupId, int levelId, int sessionId)
    {

        Group g = getGroupById(groupId);
        Diplomate d = getDiplomateById(g.diplomate);
        Level l = d.getlevelById(levelId);
        Session s = l.getSessionById(sessionId);
        return s.getProductsNames();
    }
}

public class Diplomate
{
    public int diplomate_id { get; set; }
    public Level[] levels { get; set; }
    public Level getlevelById(int id)
    {
        foreach (Level l in levels)
        {
            if (l.level_number == id)
            {
                return l;
            }

        }
        return null;
    }
}

public class Level
{
    public int level_number { get; set; }
    public Session[] sessions { get; set; }
    public Session getSessionById(int id)
    {
        foreach (Session s in sessions)
        {
            if (s.session_number == id)
            {
                return s;
            }

        }
        return null;
    }
}

public class Session
{
    public int session_number { get; set; }
    public Product[] products { get; set; }

    public List<string> getProductsNames()
    {
        List<string> evi = new List<string>();
        foreach (Product p in products)
        {
            Console.WriteLine(p.name);
            evi.Add(p.name);
        }
        return evi;
    }
}

public class Product
{
    public int product_id { get; set; }
    public string name { get; set; }
    public string type { get; set; }
}

public class Group
{
    public int id { get; set; }
    public int diplomate { get; set; }
    public Student[] students { get; set; }
}

public class Student
{
    public string name { get; set; }
    public string cc { get; set; }
    public void clickCC()
    {
        Console.WriteLine("#asa");
    }
}
public class Former
{
    public string name { get; set; }
    public string cc { get; set; }
    public string[] groups { get; set; }
}

public class Evidences
{
    public Evidences[] evidences { get; set; }
}
public class Evidence
{
    public string group_id { get; set; }
    public StudentOut[] students { get; set; }
}
public class StudentOut
{
    public string cc { get; set; }
    public ProductOut[] products { get; set; }
}
public class ProductOut
{
    public string product_id { get; set; }
    public string file_name { get; set; }
}
