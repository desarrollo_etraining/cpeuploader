﻿using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace cpeUploader
{
    /// <summary>
    /// Lógica de interacción para MenuWindow.xaml
    /// </summary>
    public partial class MenuWindow : Window
    {
        private bool hasData;
        private UploaderWindow mainData;
        private AsistenceWindow win1 = new AsistenceWindow();
        private MainWindow win2 = new MainWindow();
        private ProgressAsistenceWindow win3 = new ProgressAsistenceWindow("LISTADOS DE ASISTENCIAS");
        private ProgressEvidenceWindow win4 = new ProgressEvidenceWindow("EVIDENCIAS");

        public MenuWindow()
        {
            InitializeComponent();
            mainData = new UploaderWindow();
            mainData.init();
            if (mainData.jsonRoot == null || mainData.jsonRoot.former == null)
            {
                return;
            }
            init();
        }

        public void init()
        {
            hasData = true;
            win1.setLogic(mainData);
            win2.setLogic(mainData);
            win3.setLogic(mainData);
            win4.setLogic(mainData);
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            App.Current.Shutdown();
        }

        void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = false;
        }

        private void ver_cargue_evidencia(object sender, RoutedEventArgs e)
        {
            if (hasData)
            {
                win1.Hide();
                win3.Hide();
                win4.Hide();
                win2.Show();
                win2.refresh();
            }
        }

        private void ver_cargue_lista(object sender, RoutedEventArgs e)
        {
            if (hasData)
            {
                win2.Hide();
                win3.Hide();
                win4.Hide();
                win1.Show();
                win1.refresh();
            }

        }

        private void ver_tablero_control_asistencias(object sender, RoutedEventArgs e)
        {
            if (hasData)
            {
                win3.Show();
                win3.refresh();
                win1.Hide();
                win2.Hide();
                win4.Hide();
            }
        }

        private void ver_tablero_control_evidencias(object sender, RoutedEventArgs e)
        {
            if (hasData)
            {
                win4.Show();
                win4.refresh();
                win1.Hide();
                win2.Hide();
                win3.Hide();
            }
        }

        private void compressFiles()
        {
            exportWindow.Visibility = Visibility.Visible;
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += compressBackground;
            worker.ProgressChanged += worker_ProgressChanged;
            worker.RunWorkerAsync();
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            exportStatus.Value = e.ProgressPercentage;
            if (e.ProgressPercentage == 100)
            {
                exportWindow.Visibility = Visibility.Hidden;
                MessageBoxResult result = MessageBox.Show("Exportado con exito"
                 , "Cpe Uploader", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void compressBackground(object sender, DoWorkEventArgs e)
        {
            using (ZipFile zip = new ZipFile())
            {
                zip.AddDirectory(UploaderWindow.dirString);
                zip.CompressionLevel = Ionic.Zlib.CompressionLevel.BestCompression;
                zip.Comment = "This zip was created at " + System.DateTime.Now.ToString("G");
                zip.Save(string.Format(UploaderWindow.filePrefix + "export.zip"));
            }
             (sender as BackgroundWorker).ReportProgress(100);
        }

        private void exportButton_Click(object sender, RoutedEventArgs e)
        {
            if (hasData)
            {
                System.IO.File.Copy(UploaderWindow.outputPath, UploaderWindow.dirString + @"\data.json", true);
                compressFiles();
            }
            else
            {
                MessageBoxResult result = MessageBox.Show("No hay archivos"
                 , "Cpe Uploader", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void refreshInfo_Click(object sender, RoutedEventArgs e)
        {
            bool response = mainData.loadWebData(inputIdNumber.Text);
            if (response)
            {
                init();
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.F1))
            {
                Tutorial t = new Tutorial();
                t.Show();
            }
        }
    }
}
