﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace cpeUploader
{
    /// <summary>
    /// Lógica de interacción para Tutorial.xaml
    /// </summary>
    public partial class Tutorial : Window
    {

        private List<TutorialContent> content;
        private List<Ellipse> indicators;
        private int current = 0;
        public Tutorial()
        {
            InitializeComponent();
            content = new List<TutorialContent>();
            indicators = new List<Ellipse>();
            indicators.Add(option1);
            indicators.Add(option2);
            indicators.Add(option3);
            indicators.Add(option4);
            indicators.Add(option5);
            indicators.Add(option6);
            indicators.Add(option7);
            content.Add(new TutorialContent("En el menu principal encontraras una opcion para descargar los datos que corresponden a tus grupos"+
                " adicionalmente los 4 menus de tu aplicacion. Puedes pulsar las flechas para avanzar o devolverte en el tutorial"
                , @".\tutorials\menu.gif","Uso del Menu Principal"));
            content.Add(new TutorialContent("Para el cargue de asistencias debes seleccionar un grupo, luego el nivel y posteriomente la sesion"+
                " para la cual deseas incluir un archivo, por lo cual sera el primer paso a seguir, tras este proceso desmarca a los estudiantes"+
                " que no asistieron"
                , @".\tutorials\assistences.gif", "Uso de la vista de cargue de Asistencias"));
            content.Add(new TutorialContent("Recuerda bien guardar tus cambios dado que los cambios no guardados se perderan, asimismo"+
                " pudes usar el boton restaurar para cargar otra asistencia o evidencia segun sea el caso"
                , @".\tutorials\back_without_saving.gif", "Los cambios que no guardes se perderan"));
            content.Add(new TutorialContent("Similar a la vista para cargar asistencias deberas seleccionar un grupo, un nivel y una sesion"+
                " pero adicionalmente seleccionaras la evidencia escencial a cargar, por lo que deberas subir una evidencia por cada estudiante. "+
                " No olvides guardar tu progreso con el boton de guardar"
                , @".\tutorials\evidences.gif", "Uso de la vista para cargar Evidencias"));
            content.Add(new TutorialContent("Para las vistas de progresos de asistencias y de evidencias veras unos checkbox los cuales te indicaran"+
                " cuales estudiantes poseen evidencias cargadas y a que producto corresponden, la convencion en el nombre te ayudara a identificarlos de la siguiente manera:"+
                " N: nivel; S: sesion; P: identificador del producto; un ejemplo : N1 S1 P5 corresponde al producto 5 del nivel uno y la sesion 1"
                , @".\tutorials\progress.gif", "Vista de los progresos en los cargues de Asistencia y de Evidencias"));
            content.Add(new TutorialContent("Cuando hayas registrado el progreso de todos los estudiantes te pediremos por favor uses la funcion exportar"+
                " del menu principal, la cual servira como soporte de tu trabajo. Recuerda que esta funcion puede tomar un largo tiempo dependiendo de la cantidad"+
                " de asistencias y el peso de las mismas."
                , @".\tutorials\export.gif", "Uso de la funcion Exportar"));
            content.Add(new TutorialContent("El archivo generado por el sistema, se creara bajo el nombre export.zip y se encontrara en la carpeta donde tengas la aplicacion"
                +" no olvides hacerlo llegar a tu lider en cuanto hayas completado tu tarea."
            , @".\tutorials\file_path.gif", "Donde encontrar el archivo exportado"));
            imageHolder.Source = new Uri(content.ElementAt(current).image, UriKind.RelativeOrAbsolute);
        }

        private void imageHolder_MediaEnded(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("rewind");
            imageHolder.Position = new TimeSpan(0, 0, 0, 0, 1);
            imageHolder.Play();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            int last = current;
            if (last < 0)
            {
                last = content.Count - 1;
            }
            switch (e.Key)
            {
                case Key.Right:
                    Console.WriteLine("next called");
                    loadingImage.Visibility = Visibility.Visible;
                    current++;
                    current = current % content.Count;
                    indicators.ElementAt(last).Fill = new SolidColorBrush(Colors.White);
                    indicators.ElementAt(current).Fill = new SolidColorBrush(Colors.Black);
                    imageHolder.Source = new Uri(content.ElementAt(current).image, UriKind.RelativeOrAbsolute);
                    imageHolder.Play();
                    helpTitle.Text = content.ElementAt(current).title;
                    helpContent.Text = content.ElementAt(current).content;
                    break;
                case Key.Left:
                    Console.WriteLine("prev called");
                    loadingImage.Visibility = Visibility.Visible;
                    current--;
                    if (current < 0)
                    {
                        current = content.Count - 1;
                    }
                    indicators.ElementAt(last).Fill = new SolidColorBrush(Colors.White);
                    indicators.ElementAt(current).Fill = new SolidColorBrush(Colors.Black);
                    imageHolder.Source = new Uri(content.ElementAt(current).image, UriKind.RelativeOrAbsolute);
                    imageHolder.Play();
                    helpTitle.Text = content.ElementAt(current).title;
                    helpContent.Text = content.ElementAt(current).content;
                    break;

            }
        }

        private void imageHolder_Loaded(object sender, RoutedEventArgs e)
        {
            imageHolder.Play();
            helpTitle.Text = content.ElementAt(current).title;
            helpContent.Text = content.ElementAt(current).content;
            loadingImage.Visibility = Visibility.Hidden;
        }

        private void imageHolder_MediaOpened(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("image loadaded");
            imageHolder.Play();
            loadingImage.Visibility = Visibility.Hidden;
        }
    }

    public class TutorialContent
    {
        public TutorialContent(string content, string image,string title)
        {
            this.content = content;
            this.image = image;
            this.title = title;
        }

        public string content { get; set; }
        public string title { get; set; }
        public string image { get; set; }
    }
}
