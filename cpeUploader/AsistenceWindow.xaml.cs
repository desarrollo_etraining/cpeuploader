﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace cpeUploader
{

    public partial class AsistenceWindow : Window
    {
        private UploaderWindow logic;
        private UploaderWindow copy;
        private bool alreadyUploaded;
        private GridViewColumnHeader listViewSortCol = null;
        private SortAdorner listViewSortAdorner = null;

        public AsistenceWindow()
        {
            InitializeComponent();
        }

        public void setLogic(UploaderWindow mainData)
        {
            logic = mainData;
            logic.init();
            logic.loadData(name, cc, groupList);
            this.copy = new UploaderWindow(logic);
            groupList.ItemsSource = copy.jsonRoot.former.groups;
        }

        public void refresh()
        {
            logic.init();
            this.copy = new UploaderWindow(logic);
            groupList.ItemsSource = copy.jsonRoot.former.groups;
            restoreElements();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            this.Hide();
            e.Cancel = true;
        }

        private void groupList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0)
            {
                return;
            }
            copy.currentGroup = copy.jsonRoot.getGroupById(Int32.Parse((string)e.AddedItems[0]));
            if (copy.currentGroup.students == null || copy.currentGroup.students.Length == 0)
            {
                MessageBoxResult result = MessageBox.Show("No hay estudiantes en el grupo"
             , "Cpe Uploader", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            level.IsEnabled = true;
            copy.currentDiplomate = copy.jsonRoot.getDiplomateById(copy.currentGroup.diplomate);
            level.ItemsSource = copy.currentDiplomate.levels;
            groupList.IsEnabled = false;
        }

        private void level_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0)
            {
                return;
            }
            copy.currentLevel = ((Level)(e.AddedItems[0]));
            session.IsEnabled = true;
            session.ItemsSource = copy.currentLevel.sessions;
            level.IsEnabled = false;
        }

        private void session_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (e.AddedItems.Count == 0)
            {
                return;
            }
            boton_guardar.Visibility = Visibility.Hidden;
            studentListAsistence.Visibility = Visibility.Hidden;
            currentFileName.Visibility = Visibility.Hidden;
            Product asistencia = null;
            copy.currentSession = ((Session)(e.AddedItems[0]));
            foreach (Product p in copy.currentSession.products)
            {
                if (p.type.Contains("ASISTENCIA"))
                {
                    asistencia = p;
                }

            }
            if (asistencia == null)
            {
                MessageBoxResult result = MessageBox.Show("No se debe adjuntar ninguna asistencia"
          , "Cpe Uploader", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            boton_guardar.Visibility = Visibility.Visible;
            copy.currentEvidence = asistencia;
            studentListAsistence.IsEnabled = false;
            alreadyUploaded = isAlreadyUploaded();
            if (!alreadyUploaded)
            {
                uploadSupport.Visibility = Visibility.Visible;
                uploadSupport.IsEnabled = true;
                MessageBoxResult result = MessageBox.Show("Seleccione un archivo a cargar"
           , "Cpe Uploader", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                assitence_Click();
            }

        }

        private bool isAlreadyUploaded()
        {
            bool cond = copy.data != null && copy.data.evidences != null;
            var temp = copy.data.getEvidenceByGroupID(copy.currentGroup.group_id).getProductByID(copy.currentEvidence.product_id);
            return cond && temp != null;
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            CheckBox checkB = sender as CheckBox;
            SpecialS s = checkB.DataContext as SpecialS;
            Console.WriteLine(s.name + " check: " + checkB.IsChecked);
            if (checkB.IsChecked == true)
            {
                copy.updateStructure(s.cc, s.name, copy.currentAssistence);
            }
            else
            {
                copy.updateStructure(s.cc, s.name, "No Asiste");
            }

        }

        private void assitence_Click()
        {
            uploadSupport.Visibility = Visibility.Visible;
            uploadSupport.IsEnabled = true;
            studentListAsistence.Visibility = Visibility.Visible;
            //currentFileName.Visibility = Visibility.Visible;
            StudentOut temp;
            string fName = null;
            bool check = false;
            List<SpecialS> source = new List<SpecialS>();
            foreach (Student s in copy.currentGroup.students)
            {
                if (copy.data != null && copy.data.evidences != null)
                {
                    temp = copy.data.getEvidenceByGroupID(copy.currentGroup.group_id).getStudentByCC(s.cc);
                    fName = temp.getProductByID(copy.currentEvidence.product_id).file_name;
                    alreadyUploaded = alreadyUploaded | fName != null;

                    if (fName == null || fName == "No Asiste")
                    {
                        check = false;
                        Console.WriteLine(s.name + " no asistio");
                    }
                    else
                    {
                        check = true;
                        Console.WriteLine(s.name + " tiene " + fName);
                    }
                }
                if (!alreadyUploaded)
                {
                    source.Add(new SpecialS { name = s.name, cc = s.cc, assistence = true });
                    copy.updateStructure(s.cc, s.name, copy.currentAssistence);
                   // currentFileName.Text = "Archivo seleccionado: " + copy.currentAssistence;
                }
                else
                {
                    source.Add(new SpecialS { name = s.name, cc = s.cc, assistence = check });
                }

            }
            if (alreadyUploaded)
            {
                studentListAsistence.IsEnabled = true;
                //currentFileName.Text = "Archivo seleccionado: " + copy.currentAssistence;
            }
            studentListAsistence.ItemsSource = source;

        }

        private void uploadSupport_Click(object sender, RoutedEventArgs e)
        {
            if (alreadyUploaded)
            {
                MessageBoxResult result = MessageBox.Show("Sobre escribira la Lista Actual ¿Desea Continuar?"
                    , "Cpe Uploader", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.No)
                {
                    return;
                }
                assitence_Click();
            }
            copy.currentAssistence = copy.selectFile(false, "", copy.currentGroup);
            studentListAsistence.IsEnabled = true;
            uploadSupport.IsEnabled = false;
            assitence_Click();
        }

        private void restore_Click(object sender, RoutedEventArgs e)
        {
            restoreElements();
            refresh();
        }

        private void restoreElements()
        {
            groupList.IsEnabled = true;
            groupList.SelectedIndex = -1;
            level.IsEnabled = false;
            level.SelectedIndex = -1;
            session.IsEnabled = false;
            session.SelectedIndex = -1;
            alreadyUploaded = false;
            studentListAsistence.Visibility = Visibility.Hidden;
            currentFileName.Visibility = Visibility.Hidden;
            boton_guardar.Visibility = Visibility.Hidden;
            uploadSupport.Visibility = Visibility.Hidden;
        }
        private void boton_guardar_Click(object sender, RoutedEventArgs eventArg)
        {
            if (copy.currentAssistence==null)
            {
                MessageBoxResult result = MessageBox.Show("no has seleccionado ningun archivo"
                   , "Cpe Uploader", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            copy.writeFile();
            logic = copy;
            Console.WriteLine("file saved");
        }

        public class SpecialS
        {
            public string name { get; set; }
            public string cc { get; set; }
            public bool assistence { get; set; }
        }

        private void cerrar_pantalla_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }

        private void GridViewColumnHeader_Click(object sender, RoutedEventArgs e)
        {

            GridViewColumnHeader column = (sender as GridViewColumnHeader);
            string sortBy = column.Tag.ToString();
            if (listViewSortCol != null)
            {
                AdornerLayer.GetAdornerLayer(listViewSortCol).Remove(listViewSortAdorner);
                studentListAsistence.Items.SortDescriptions.Clear();
            }

            ListSortDirection newDir = ListSortDirection.Ascending;
            if (listViewSortCol == column && listViewSortAdorner.Direction == newDir)
                newDir = ListSortDirection.Descending;

            listViewSortCol = column;
            listViewSortAdorner = new SortAdorner(listViewSortCol, newDir);
            AdornerLayer.GetAdornerLayer(listViewSortCol).Add(listViewSortAdorner);
            studentListAsistence.Items.SortDescriptions.Add(new SortDescription(sortBy, newDir));

            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(studentListAsistence.ItemsSource);
            view.SortDescriptions.Add(new SortDescription(sortBy, ListSortDirection.Ascending));

        }
    }
}
