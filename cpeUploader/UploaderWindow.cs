﻿using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace cpeUploader
{
    public class UploaderWindow
    {
        public UploaderWindow()
        {
            this.filesToCopy = new Dictionary<string, string>();
        }
        public UploaderWindow(UploaderWindow toClone)
        {
            this.filesToCopy = new Dictionary<string, string>();
            this.currentAssistence = toClone.currentAssistence;
            this.data = new Evidences(toClone.data);
            this.jsonRoot = toClone.jsonRoot;
        }
        public Evidences data;
        public Root jsonRoot;
        public Level currentLevel;
        public Session currentSession;
        public Product currentEvidence = null;
        public Group currentGroup;
        public Diplomate currentDiplomate;
        public static string filePrefix = @"";
        public static string path = filePrefix + "input.json";
        public static string outputPath = filePrefix + "data.json";
        public static string dirString = filePrefix + "files";
        public string currentAssistence;

        private Dictionary<string, string> filesToCopy;

        public void init()
        {
            filesToCopy.Clear();
            JsonSerializer serializer = new JsonSerializer();
            if (File.Exists(path))
            {
                using (StreamReader re = new StreamReader(path))
                {
                    JsonTextReader reader = new JsonTextReader(re);
                    jsonRoot = serializer.Deserialize<Root>(reader);
                    Console.WriteLine(jsonRoot.former.name);
                }
            }

            if (File.Exists(outputPath))
            {
                using (StreamReader re = new StreamReader(outputPath))
                {
                    JsonTextReader reader = new JsonTextReader(re);
                    data = serializer.Deserialize<Evidences>(reader);
                }
            }
        }

        public bool loadWebData(string cc)
        {
            string json = "";
            try
            {       
                using (WebClient wc = new WebClient())
                {
                    json = wc.DownloadString("http://104.198.252.81/cpe_repo/groups/download_data.php?former_id=" + cc);
                    json=Encoding.UTF8.GetString(Encoding.Default.GetBytes(json));
                }

                if (json != null || json != "")
                {
                    JsonSerializer serializer = new JsonSerializer();
                    JsonTextReader reader = new JsonTextReader(new StringReader(json));
                    json = json.Trim();
                    Console.WriteLine(json);
                    Root temp = serializer.Deserialize<Root>(reader);
                    if (temp.former.name == null)
                    {
                        MessageBoxResult resultBad = MessageBox.Show("Los datos son incorrectos"
                     , "Cpe Uploader", MessageBoxButton.OK, MessageBoxImage.Error);
                        return false;
                    }
                    MessageBoxResult result = MessageBox.Show("Se han actualizado los datos"
                    , "Cpe Uploader", MessageBoxButton.OK, MessageBoxImage.Information);
                    File.WriteAllText(path, json);
                }
                init();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                MessageBoxResult result = MessageBox.Show("No se ha podido conectar con el servidor"
                   , "Cpe Uploader", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }
            return true;
        }

        public void loadData(Label name, Label cc, ComboBox groupList)
        {
            name.Content = "Nombre: " + jsonRoot.former.name;
            cc.Content = "Cedula: " + jsonRoot.former.cc;
            groupList.ItemsSource = jsonRoot.former.groups;
        }

        public static object GetDynamicObject(Dictionary<string, object> properties)
        {
            var dynamicObject = new ExpandoObject() as IDictionary<string, Object>;

            foreach (var property in properties)
            {
                dynamicObject.Add(property.Key, property.Value);
            }
            return (object)dynamicObject;
        }

        public delegate string ToStringFunc();

        public static void addProduct(ProductOut p, List<string> uniqueProducts
        , Dictionary<string, string> columnTitles, Dictionary<string, object> properties)
        {
            if (!uniqueProducts.Contains("p_" + p.product_id))
            {
                uniqueProducts.Add("p_" + p.product_id);
                columnTitles.Add("p_" + p.product_id, p.name);
            }
            if (p.name != null)
            {
                if (p.file_name == null)
                {
                    properties.Add("p_" + p.product_id, false);
                }
                else
                {
                    properties.Add("p_" + p.product_id, p.file_name != "No Asiste");
                }
            }
        }

        public void writeFile()
        {
            string json = JsonConvert.SerializeObject(data);
            foreach (string k in filesToCopy.Keys)
            {
                System.IO.File.Copy(filesToCopy[k], k, true);
            }

            File.WriteAllText(outputPath, json);
            MessageBoxResult result = MessageBox.Show("Guardado con exito"
                  , "Cpe Uploader", MessageBoxButton.OK, MessageBoxImage.Information);
            filesToCopy = new Dictionary<string, string>();
        }

        public void updateStructure(string cc, string name, string fileName)
        {
            if (fileName == null)
            {
                Console.WriteLine("nombre de archivo vacio");
                //return;
            }
            if (data == null)
            {
                data = new Evidences();
            }
            Evidence e = data.getEvidenceByGroupID(currentGroup.group_id);
            e.diplomate = currentGroup.diplomate;
            StudentOut s = e.getStudentByCC(cc);
            s.name = name;
            ProductOut p = s.getProductByID(currentEvidence.product_id);
            p.file_name = fileName;
            string lvl = currentLevel.level_number.ToString().Last() + "";
            string session = currentSession.session_number.ToString().Last() + "";
            string productName = currentEvidence.product_id;
            p.name = "N" + lvl + " S" + session + " P " + productName;
            p.type = currentEvidence.type;
            Console.WriteLine(s.getProductByID(currentEvidence.product_id).file_name);
        }

        public string selectFile(Boolean tipo_evidencia, string cc, Group grupo)
        {
            // Create an instance of the open file dialog box.
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.Filter = jsonRoot.supportedFiles;
            openFileDialog1.FilterIndex = 1;

            // Call the ShowDialog method to show the dialog box.
            bool? userClickedOK = openFileDialog1.ShowDialog();

            // Process input if the user clicked OK.
            if (userClickedOK == true)
            {
                if (!Directory.Exists(dirString))
                {
                    Directory.CreateDirectory(dirString);
                }
                string fName = openFileDialog1.SafeFileName;
                string substring = "." + fName.Split('.')[1];
                string newFileName = @"\" + currentEvidence.product_id + "_";
                // La evidencias es individual
                if (tipo_evidencia)
                {
                    newFileName += cc + substring;
                }
                else
                {
                    newFileName += grupo.group_id + substring;
                }

                Console.WriteLine(fName);
                if (!filesToCopy.ContainsKey(dirString + newFileName))
                {
                    filesToCopy.Add(dirString + newFileName, openFileDialog1.FileName);
                }
                else
                {
                    filesToCopy[dirString + newFileName] = openFileDialog1.FileName;
                }
                return newFileName;
            }
            return null;
        }

    }
}
