﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace cpeUploader
{
    public class Root
    {
        public Former former { get; set; }
        public List<Group> groups { get; set; }
        public List<Diplomate> diplomates { get; set; }
        public string supportedFiles { get; set; }

        public Group getGroupById(int id)
        {
            foreach (Group g in groups)
            {
                if (g.group_id == id)
                {
                    return g;
                }

            }
            return null;
        }
        public Diplomate getDiplomateById(int id)
        {
            foreach (Diplomate d in diplomates)
            {
                if (d.diplomate_id == id)
                {
                    return d;
                }

            }
            return null;
        }

    }

    public class Diplomate
    {
        public int diplomate_id { get; set; }
        public List<Level> levels { get; set; }
        public Level getlevelById(string id)
        {
            foreach (Level l in levels)
            {
                if (l.level_number == id)
                {
                    return l;
                }

            }
            return null;
        }
    }

    public class Level
    {
        public string level_number { get; set; }
        public List<Session> sessions { get; set; }
        public Session getSessionById(string id)
        {
            foreach (Session s in sessions)
            {
                if (s.session_number == id)
                {
                    return s;
                }

            }
            return null;
        }
        public override string ToString()
        {
            return level_number;
        }
    }

    public class Session
    {
        public string session_number { get; set; }
        public List<Product> products { get; set; }

        public Product getProductById(string id)
        {
            foreach (Product p in products)
            {
                if (p.product_id==id)
                {
                    return p;
                }
            }
            return null;
        }

        public override string ToString()
        {
            return session_number;
        }

    }

    public class Product
    {
        public string product_id { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public override string ToString()
        {
            return name;
        }
    }

    public class Group
    {
        public int group_id { get; set; }
        public int diplomate { get; set; }
        public Student[] students { get; set; }
    }

    public class Student
    {
        public string name { get; set; }
        public string cc { get; set; }
        public string fileName { get; set; }
    }
    public class Former
    {
        public string name { get; set; }
        public string cc { get; set; }
        public string[] groups { get; set; }
    }

    public class Evidences
    {
        public Evidences()
        {

        }
        public Evidences(Evidences toClone)
        {
            if (toClone == null)
            {
                return;
            }
            this.evidences = new List<Evidence>();
            foreach (Evidence e in toClone.evidences)
            {
                this.evidences.Add(new Evidence(e));
            }
        }
        public List<Evidence> evidences { get; set; }

        public Evidence getEvidenceByGroupID(int group) {
            if (evidences==null)
            {
                evidences = new List<Evidence>();
            }
            foreach (Evidence e in evidences)
            {
                if (e.group_id == group)
                {
                    return e;
                }
            }

            Evidence ne = new Evidence();
            ne.group_id = group;
            evidences.Add(ne);
            return ne;
        }
    }
    public class Evidence
    {
        public Evidence()
        {

        }

        public Evidence(Evidence toClone)
        {
            if (toClone == null||toClone.students==null)
            {
                return;
            }
            this.group_id = toClone.group_id;
            this.diplomate = toClone.diplomate;
            this.students = new List<StudentOut>();
            foreach(StudentOut s in toClone.students)
            {
                this.students.Add(new StudentOut(s));
            }
        }
        public int group_id { get; set; }
        public int diplomate { get; set; }
        public List<StudentOut> students { get; set; }
        public StudentOut getStudentByCC(string cedula)
        {
            if (students==null)
            {
                students = new List<StudentOut>();
            }
            foreach (StudentOut e in students)
            {
                if (e.cc == cedula)
                {
                    return e;
                }
            }
            StudentOut s= new StudentOut();
            students.Add(s);
            s.cc = cedula;
            return s ;
        }

        public ProductOut getProductByID(string product_id)
        {
            if (students == null)
            {
                return null;
            }
            foreach (StudentOut s in students)
            {
                if (s.products==null)
                {
                    return null;
                }
                foreach (ProductOut p in s.products)
                {
                    if (p.product_id==product_id)
                    {
                        return p;
                    }
                }
            }
            return null;
        }

    }
    public class StudentOut
    {
        public StudentOut()
        {

        }
        public StudentOut(StudentOut toClone)
        {
            if (toClone == null)
            {
                return;
            }
            this.name = toClone.name;
            this.cc = toClone.cc;
            this.products = new List<ProductOut>();
            foreach (ProductOut p in toClone.products)
            {
                this.products.Add(new ProductOut(p));
            }
        }
        public string name { get; set; }
        public string cc { get; set; }
        public List<ProductOut> products { get; set; }

        public override string ToString()
        {
            return cc;
        }

        public ProductOut getProductByID(string producto)
        {
            if (products==null)
            {
                products = new List<ProductOut>();
            }
            foreach (ProductOut p in products)
            {
                if (p.product_id == producto)
                {
                    return p;
                }
            }
            ProductOut pn= new ProductOut();
            pn.product_id = producto;
            products.Add(pn);
            return pn;
        }
    }
    public class ProductOut
    {
        public ProductOut()
        {

        }
        public ProductOut(ProductOut toClone)
        {
            if (toClone == null)
            {
                return;
            }
            this.product_id = toClone.product_id;
            this.file_name = toClone.file_name;
            this.name = toClone.name;
            this.type = toClone.type;
        }
        public string product_id { get; set; }
        public string file_name { get; set; }
        public string name { get; set; }
        public string type { get; set; }
    }
    public class SortAdorner : Adorner
    {
        private static Geometry ascGeometry =
                Geometry.Parse("M 0 4 L 3.5 0 L 7 4 Z");

        private static Geometry descGeometry =
                Geometry.Parse("M 0 0 L 3.5 4 L 7 0 Z");

        public ListSortDirection Direction { get; private set; }

        public SortAdorner(UIElement element, ListSortDirection dir)
                : base(element)
        {
            this.Direction = dir;
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            if (AdornedElement.RenderSize.Width < 20)
                return;

            TranslateTransform transform = new TranslateTransform
                    (
                            AdornedElement.RenderSize.Width - 15,
                            (AdornedElement.RenderSize.Height - 5) / 2
                    );
            drawingContext.PushTransform(transform);

            Geometry geometry = ascGeometry;
            if (this.Direction == ListSortDirection.Descending)
                geometry = descGeometry;
            drawingContext.DrawGeometry(Brushes.Black, null, geometry);

            drawingContext.Pop();
        }
    }
}
