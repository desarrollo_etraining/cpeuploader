﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;
using System.IO;
using Ionic.Zip;
using Microsoft.Win32;

namespace cpeUploader
{
    /// <summary>
    /// Lógica de interacción para ProgressEvidenceWindow.xaml
    /// </summary>
    public partial class ProgressEvidenceWindow : Window
    {
        private UploaderWindow logic;

        public ProgressEvidenceWindow(string titulo)
        {
            InitializeComponent();
            init(titulo);
        }

        public void refresh()
        {
            GroupList.SelectedIndex = -1;
            progessList.Items.Clear();
            logic.init();
            GroupList.ItemsSource = logic.jsonRoot.former.groups;
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            this.Hide();
            e.Cancel = true;
        }

        public void init(string titulo)
        {
            var tituloVentana = (TextBlock)this.FindName("tituloVentana");
            tituloVentana.Text = "PROGRESO " + titulo;
        }

        public void setLogic(UploaderWindow mainData)
        {
            logic = new UploaderWindow();
            logic.init();
            GroupList.ItemsSource = logic.jsonRoot.former.groups;
        }

        public void setData(Evidence data)
        {
            progessList.Items.Clear();
            GridView myGridView = new GridView();
            myGridView.ColumnHeaderToolTip = "Informacion del Estudiante";

            GridViewColumn gvc1 = new GridViewColumn();
            gvc1.DisplayMemberBinding = new Binding("cc");
            gvc1.Header = "Cedula";
            myGridView.Columns.Add(gvc1);
            GridViewColumn gvc2 = new GridViewColumn();
            gvc2.DisplayMemberBinding = new Binding("name");
            gvc2.Header = "Nombre";
            myGridView.Columns.Add(gvc2);
            List<string> uniqueProducts = new List<string>();
            var columnTitles = new Dictionary<string, string>();
            if (data.students == null)
            {
                MessageBoxResult result = MessageBox.Show("No hay datos registrados"
               , "Cpe Uploader", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            foreach (StudentOut s in data.students)
            {
                var properties = new Dictionary<string, object>();
                properties.Add("cc", s.cc);
                if (s.name != null)
                {
                    properties.Add("name", s.name);
                    foreach (ProductOut p in s.products)
                    {
                        if (p.type != null)
                        {
                            bool isAssistence = p.type.Contains("ASISTENCIA");
                            if (!isAssistence)
                            {
                                UploaderWindow.addProduct(p, uniqueProducts, columnTitles, properties);
                            }
                        }
                    }
                    var obj = UploaderWindow.GetDynamicObject(properties);
                    progessList.Items.Add(obj);
                }
            }
            foreach (string ps in uniqueProducts)
            {
                GridViewColumn gv = new GridViewColumn();
                DataTemplate template = new DataTemplate();

                FrameworkElementFactory factory = new FrameworkElementFactory(typeof(Grid));
                template.VisualTree = factory;
                FrameworkElementFactory imgFactory = new FrameworkElementFactory(typeof(CheckBox));

                Binding newBinding = new Binding(ps);
                imgFactory.SetBinding(CheckBox.IsCheckedProperty, newBinding);
                imgFactory.SetValue(CheckBox.IsEnabledProperty, false);

                factory.AppendChild(imgFactory);
                gv.CellTemplate = template;
                gv.Header = columnTitles[ps];
                myGridView.Columns.Add(gv);
            }

            progessList.View = myGridView;
        }

        private void GroupList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0)
            {
                return;
            }
            logic.currentGroup = logic.jsonRoot.getGroupById(Int32.Parse((string)e.AddedItems[0]));
            if (logic.data == null)
            {
                MessageBoxResult result = MessageBox.Show("No hay datos registrados"
                , "Cpe Uploader", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            setData(logic.data.getEvidenceByGroupID(logic.currentGroup.group_id));
        }

        private void cerrar_pantalla_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }

    }
}
