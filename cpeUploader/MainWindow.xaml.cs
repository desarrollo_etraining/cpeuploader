﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Newtonsoft.Json;
using System.IO;
using Ionic.Zip;
using Microsoft.Win32;
using System.Windows.Documents;
using System.ComponentModel;
using System.Windows.Data;

namespace cpeUploader
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private UploaderWindow logic;
        private UploaderWindow copy;
        private GridViewColumnHeader listViewSortCol = null;
        private SortAdorner listViewSortAdorner = null;

        public MainWindow()
        {
            InitializeComponent();
        }

        public void setLogic(UploaderWindow mainData)
        {
            logic = mainData;
            copy = new UploaderWindow(logic);
            logic.init();
            logic.loadData(name, cc, GroupList);
            GroupList.ItemsSource = logic.jsonRoot.former.groups;
        }

        public void refresh()
        {
            logic.init();
            this.copy = new UploaderWindow(logic);
            GroupList.ItemsSource = copy.jsonRoot.former.groups;
            restoreElements();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            this.Hide();
            e.Cancel = true;
        }

        private void GroupList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0)
            {
                return;
            }
            copy.currentGroup = copy.jsonRoot.getGroupById(Int32.Parse((string)e.AddedItems[0]));
            if (copy.currentGroup.students == null || copy.currentGroup.students.Length == 0)
            {
                MessageBoxResult result = MessageBox.Show("No hay estudiantes en el grupo"
             , "Cpe Uploader", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            level.IsEnabled = true;
            copy.currentDiplomate = copy.jsonRoot.getDiplomateById(copy.currentGroup.diplomate);
            level.ItemsSource = copy.currentDiplomate.levels;
            GroupList.IsEnabled = false;

        }

        private void loadTableData(Group group, Level level, Session session, Product evidence)
        {
            StudentOut temp;
            string fName = null;
            List<Student> newStudents = new List<Student>();
            foreach (Student s in group.students)
            {
                if (copy.data != null)
                {
                    temp = copy.data.getEvidenceByGroupID(group.group_id).getStudentByCC(s.cc);
                    fName = temp.getProductByID(evidence.product_id).file_name;
                    s.fileName = temp.getProductByID(evidence.product_id).file_name;

                }
                newStudents.Add(new Student { name = s.name, cc = s.cc, fileName = fName });
            }
            studentList.ItemsSource = newStudents;
        }



        private void level_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0)
            {
                return;
            }
            copy.currentLevel = ((Level)(e.AddedItems[0]));
            session.IsEnabled = true;
            session.ItemsSource = copy.currentLevel.sessions;
            level.IsEnabled = false;
        }

        private void session_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            comboEvidences.Items.Clear();
            if (e.AddedItems.Count == 0)
            {
                return;
            }
            copy.currentSession = ((Session)(e.AddedItems[0]));
            session.IsEnabled = false;
            foreach (Product p in copy.currentSession.products)
            {
                if (p.type != "ASISTENCIA")
                {
                    comboEvidences.Items.Add(p);
                }

            }
            if (comboEvidences.Items.Count>0)
            {
                comboEvidences.IsEnabled = true;
            }else
            {
                MessageBoxResult result = MessageBox.Show("No hay productos para esta sesion. Por favor restaura la vista"
           , "Cpe Uploader", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            Student s = button.DataContext as Student;
            Console.WriteLine(s.name);
            string fName = copy.selectFile(true, s.cc, null);
            copy.currentAssistence = fName;
            copy.updateStructure(s.cc, s.name, fName);
            s.fileName = fName;
            studentList.Items.Refresh();
        }

        private void delete_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            Student s = button.DataContext as Student;
            Console.WriteLine(s.name);
            string fName = null;
            copy.currentAssistence = fName;
            copy.updateStructure(s.cc, s.name, fName);
            s.fileName = fName;
            studentList.Items.Refresh();
        }

        private void comboEvidences_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            boton_guardar.Visibility = Visibility.Visible;
            if (e.AddedItems.Count == 0)
            {
                return;
            }
            copy.currentEvidence = ((Product)e.AddedItems[0]);
            studentList.Visibility = Visibility.Visible;
            loadTableData(copy.currentGroup, copy.currentLevel, copy.currentSession, copy.currentEvidence);
        }

        private void restore_Click(object sender, RoutedEventArgs e)
        {
            restoreElements();
        }

        public void restoreElements()
        {
            GroupList.IsEnabled = true;
            GroupList.SelectedIndex = -1;
            level.IsEnabled = false;
            level.SelectedIndex = -1;
            session.IsEnabled = false;
            session.SelectedIndex = -1;
            comboEvidences.IsEnabled = false;
            comboEvidences.SelectedIndex = -1;
            boton_guardar.Visibility = Visibility.Hidden;
            studentList.Visibility = Visibility.Hidden;
        }

        private void boton_guardar_Click(object sender, RoutedEventArgs eventArg)
        {
            if (copy.currentAssistence == null)
            {
                MessageBoxResult result = MessageBox.Show("no has seleccionado ningun archivo"
                   , "Cpe Uploader", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            copy.writeFile();
            logic = copy;
            Console.WriteLine("file saved");
        }


        private void cerrar_pantalla_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }

        private void GridViewColumnHeader_Click(object sender, RoutedEventArgs e)
        {

            GridViewColumnHeader column = (sender as GridViewColumnHeader);
            string sortBy = column.Tag.ToString();
            if (listViewSortCol != null)
            {
                AdornerLayer.GetAdornerLayer(listViewSortCol).Remove(listViewSortAdorner);
                studentList.Items.SortDescriptions.Clear();
            }

            ListSortDirection newDir = ListSortDirection.Ascending;
            if (listViewSortCol == column && listViewSortAdorner.Direction == newDir)
                newDir = ListSortDirection.Descending;

            listViewSortCol = column;
            listViewSortAdorner = new SortAdorner(listViewSortCol, newDir);
            AdornerLayer.GetAdornerLayer(listViewSortCol).Add(listViewSortAdorner);
            studentList.Items.SortDescriptions.Add(new SortDescription(sortBy, newDir));

            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(studentList.ItemsSource);
            view.SortDescriptions.Add(new SortDescription(sortBy, ListSortDirection.Ascending));

        }
    }
}
